import { UserListState } from './user-list/user-list.reducer';

export interface AppState {
  userList: UserListState;
}
