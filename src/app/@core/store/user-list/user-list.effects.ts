import { Injectable } from '@angular/core';
import { UsersService } from '@app/@shared/models/users/users.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { LoadUserList, LoadUserListError, LoadUserListSuccess } from './user-list.actions';
import { UserListState } from './user-list.reducer';

// const log = new Logger('ProductListEffects');

@Injectable()
export class UserListEffects {
  constructor(
    private actions$: Actions,
    private store$: Store<{ userList: UserListState }>,
    private usersService: UsersService
  ) {}

  loadUserList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LoadUserList),
      mergeMap((action) =>
        this.usersService.findUsers(action.param).pipe(
          map((result) => LoadUserListSuccess({ result })),
          catchError(() => of(LoadUserListError))
        )
      )
    );
  });
}
