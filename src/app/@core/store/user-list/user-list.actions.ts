import { User } from '@app/@shared/models/users/user.interface';
import { createAction, props } from '@ngrx/store';
import { PaginateResult } from '@shared/_dtos/paginate-result';
import { FindUsersParam } from '@shared/_dtos/users/find-users.param';

export const LoadUserList = createAction('[User List] Load User List', props<{ param: FindUsersParam }>());

export const LoadUserListSuccess = createAction(
  '[User List] Load User List Success',
  props<{ result: PaginateResult<User> }>()
);

export const LoadUserListError = createAction('[User List] Load User List Error', props<{ error: any }>());
