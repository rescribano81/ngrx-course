import { User } from '@app/@shared/models/users/user.interface';
import { Action, createReducer, on } from '@ngrx/store';
import { LoadUserList, LoadUserListError, LoadUserListSuccess } from './user-list.actions';

export interface UserListState {
  docs: Array<User>;
  isLoading: boolean;
  lastError?: any;
}

export const initialState: UserListState = {
  docs: new Array<User>(),
  isLoading: false,
} as UserListState;

const UserListReducer = createReducer(
  initialState,
  on(LoadUserList, (state) => ({
    ...state,
    isLoading: true,
  })),

  on(LoadUserListSuccess, (state, { result }) => ({
    ...state,
    docs: result.docs,
    isLoading: false,
  })),

  on(LoadUserListError, (state, { error }) => ({
    ...state,
    isLoading: false,
    lastError: error,
  }))
);

export function userListReducer(state: UserListState | undefined, action: Action) {
  return UserListReducer(state, action);
}
