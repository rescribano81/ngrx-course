import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@app/@shared';
import { MaterialModule } from '@app/material.module';
import { ShopListWithNgrxComponent } from './shop-list-with-ngrx/shop-list-with-ngrx.component';
import { ShopListWithoutNgrxComponent } from './shop-list-without-ngrx/shop-list-without-ngrx.component';
import { ShopsRoutingModule } from './shops-routing.module';
import { ShopsComponent } from './shops.component';

@NgModule({
  declarations: [ShopsComponent, ShopListWithoutNgrxComponent, ShopListWithNgrxComponent],
  imports: [CommonModule, SharedModule, FlexLayoutModule, MaterialModule, ShopsRoutingModule],
})
export class ShopsModule {}
