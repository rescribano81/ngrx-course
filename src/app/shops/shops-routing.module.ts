import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopListWithNgrxComponent } from './shop-list-with-ngrx/shop-list-with-ngrx.component';
import { ShopListWithoutNgrxComponent } from './shop-list-without-ngrx/shop-list-without-ngrx.component';
import { ShopsComponent } from './shops.component';

const routes: Routes = [
  {
    path: '',
    component: ShopsComponent,
    children: [
      {
        path: 'shop-list-without-ngrx',
        component: ShopListWithoutNgrxComponent,
      },
      {
        path: 'shop-list-with-ngrx',
        component: ShopListWithNgrxComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopsRoutingModule {}
