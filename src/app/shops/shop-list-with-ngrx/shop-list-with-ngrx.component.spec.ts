import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopListWithNgrxComponent } from './shop-list-with-ngrx.component';

describe('ShopListWithNgrxComponent', () => {
  let component: ShopListWithNgrxComponent;
  let fixture: ComponentFixture<ShopListWithNgrxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShopListWithNgrxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopListWithNgrxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
