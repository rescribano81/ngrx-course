import { Component, OnInit } from '@angular/core';
import { Shop } from '@app/@shared/models/shops/shop';
import { ShopsService } from '@app/@shared/models/shops/shops.service';
import { PaginateParam } from '@app/@shared/_dtos/paginate.param';

@Component({
  selector: 'app-shop-list-without-ngrx',
  templateUrl: './shop-list-without-ngrx.component.html',
  styleUrls: ['./shop-list-without-ngrx.component.scss'],
})
export class ShopListWithoutNgrxComponent implements OnInit {
  shopList?: Shop[];
  constructor(private shopsService: ShopsService) {}

  ngOnInit(): void {}

  loadData(): void {
    const param = new PaginateParam();
    this.shopsService.findShops(param).subscribe((result) => {
      this.shopList = result.docs;
    });
  }
}
