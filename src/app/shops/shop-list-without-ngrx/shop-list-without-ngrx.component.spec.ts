import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopListWithoutNgrxComponent } from './shop-list-without-ngrx.component';

describe('ShopListWithoutNgrxComponent', () => {
  let component: ShopListWithoutNgrxComponent;
  let fixture: ComponentFixture<ShopListWithoutNgrxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShopListWithoutNgrxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopListWithoutNgrxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
