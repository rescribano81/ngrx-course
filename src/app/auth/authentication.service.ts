import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Credentials, CredentialsService } from './credentials.service';

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private credentialsService: CredentialsService, private http: HttpClient) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    const { remember, ...param } = context;

    const url = '/auth/login';
    return this.http.post<{ token: string }>(url, param).pipe(
      map((result) => {
        const data = {
          username: context?.username,
          token: result?.token,
        } as Credentials;
        this.credentialsService.setCredentials(data, context?.remember);
        return data;
      })
    );
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }

  public getCredentials(): Credentials {
    const credentialsAsString = localStorage.getItem('credentials');
    if (credentialsAsString) {
      const credentials = JSON.parse(credentialsAsString) as Credentials;
      return credentials;
    }
    // return null;
  }

  public getToken(): string {
    const credentials = this.getCredentials();
    if (credentials) {
      return credentials.token;
    }
    // return null;
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting
    // whether or not the token is expired

    const helper = new JwtHelperService();

    //const decodedToken = helper.decodeToken(myRawToken);
    //const expirationDate = helper.getTokenExpirationDate(myRawToken);
    const isExpired = helper.isTokenExpired(token);

    return !isExpired;
  }
}
