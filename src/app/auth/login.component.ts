import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger, UntilDestroy, untilDestroyed } from '@core';
import { environment } from '@env/environment';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { CredentialsService } from './credentials.service';

const log = new Logger('Login');

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  isLoading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private credentialsService: CredentialsService,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.createForm();
  }

  ngOnInit() {
    const redirect = this.route.snapshot.queryParams.redirect;
    const token = this.route.snapshot.paramMap.get('token');
    const username = this.route.snapshot.paramMap.get('username');
    if (token) {
      // console.log('token:', token);
      // console.log('username:', username);
      console.log('credentials2', { username, token });
      this.credentialsService.setCredentials({ username, token });
      this.router.navigate([redirect || '/'], { replaceUrl: true });
    }
  }

  login() {
    this.isLoading = true;
    const login$ = this.authenticationService.login(this.loginForm.value);
    login$
      .pipe(
        finalize(() => {
          this.loginForm.markAsPristine();
          this.isLoading = false;
        }),
        untilDestroyed(this)
      )
      .subscribe(
        (credentials) => {
          log.debug(`${credentials?.username} successfully logged in`);
          console.log('credentials', credentials);
          this.credentialsService.setCredentials(credentials);
          this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
        },
        (error) => {
          log.debug(`Login error: ${error}`);
          this.error = error;
        }
      );
  }

  onSignInWithGoogle() {
    //TODO: Custom url with enviroment values

    this.document.location.href = '/api/auth/google';
    //window.r
    //console.log('google')
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true,
    });
  }
}
