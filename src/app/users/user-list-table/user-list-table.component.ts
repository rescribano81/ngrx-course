import { Component, Input, OnInit } from '@angular/core';
import { User } from '@shared/models/users/user.interface';

@Component({
  selector: 'app-user-list-table',
  templateUrl: './user-list-table.component.html',
  styleUrls: ['./user-list-table.component.scss'],
})
export class UserListTableComponent implements OnInit {
  @Input() userList: User[];

  readonly displayedColumns = ['photo', 'username', 'email', 'alias'];

  constructor() {}

  ngOnInit(): void {}
}
