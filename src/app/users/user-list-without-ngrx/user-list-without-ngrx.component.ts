import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Logger } from '@core';
import { User } from '@app/@shared/models/users/user.interface';
import { UsersService } from '@app/@shared/models/users/users.service';
import { PaginateResult } from '@app/@shared/_dtos/paginate-result';
import { PaginateParam } from '@app/@shared/_dtos/paginate.param';

const log = new Logger('UserListWithoutNgrxComponent');

@Component({
  selector: 'app-user-list-without-ngrx',
  templateUrl: './user-list-without-ngrx.component.html',
  styleUrls: ['./user-list-without-ngrx.component.scss'],
})
export class UserListWithoutNgrxComponent implements OnInit {
  isLoading = false;
  userList?: User[];
  page = 1;
  limit = 10;
  total = 0;

  constructor(private usersService: UsersService) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    const param = new PaginateParam();
    param.limit = this.limit;
    param.page = this.page;

    this.usersService.findUsers(param).subscribe({
      next: (result: PaginateResult<User>) => {
        this.userList = result.docs;
        this.limit = result.limit;
        this.page = result.page;
        this.total = result.totalDocs;
      },
      error: (error) => {
        log.error(error);
      },
      complete: () => {
        this.isLoading = true;
      },
    });
  }

  onPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.limit = event.pageSize;

    this.loadData();
  }
}
