import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListWithoutNgrxComponent } from './user-list-without-ngrx.component';

describe('UserListWithoutNgrxComponent', () => {
  let component: UserListWithoutNgrxComponent;
  let fixture: ComponentFixture<UserListWithoutNgrxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserListWithoutNgrxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListWithoutNgrxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
