import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@shared';
import { MaterialModule } from '@app/material.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserListTableComponent } from './user-list-table/user-list-table.component';
import { UserListWithoutNgrxComponent } from './user-list-without-ngrx/user-list-without-ngrx.component';
import { UserListWithNgrxComponent } from './user-list-with-ngrx/user-list-with-ngrx.component';

@NgModule({
  imports: [CommonModule, TranslateModule, SharedModule, FlexLayoutModule, MaterialModule, UsersRoutingModule],
  declarations: [UsersComponent, UserListTableComponent, UserListWithoutNgrxComponent, UserListWithNgrxComponent],
})
export class UsersModule {}
