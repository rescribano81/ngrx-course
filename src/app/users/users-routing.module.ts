import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';
import { UserListWithNgrxComponent } from './user-list-with-ngrx/user-list-with-ngrx.component';
import { UserListWithoutNgrxComponent } from './user-list-without-ngrx/user-list-without-ngrx.component';
import { UsersComponent } from './users.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: '',
      redirectTo: '/users',
      pathMatch: 'full',
    },
    {
      path: 'users',
      component: UsersComponent,
      data: { title: marker('Users') },
      children: [
        {
          path: 'user-list-without-ngrx',
          component: UserListWithoutNgrxComponent,
        },
        {
          path: 'user-list-with-ngrx',
          component: UserListWithNgrxComponent,
        },
      ],
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class UsersRoutingModule {}
