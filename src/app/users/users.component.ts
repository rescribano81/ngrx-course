import { Component, OnInit } from '@angular/core';
import { Logger } from '@app/@core';

const log = new Logger('UsersComponent');
@Component({
  selector: 'app-home',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
