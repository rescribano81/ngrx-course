import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Logger } from '@core';
import { AppState } from '@core/store/app.state';
import { LoadUserList } from '@core/store/user-list/user-list.actions';
import { UserListState } from '@core/store/user-list/user-list.reducer';
import { Store } from '@ngrx/store';
import { FindUsersParam } from '@shared/_dtos/users/find-users.param';
import { Observable } from 'rxjs';

const log = new Logger('UserListWithNgrxComponent');
@Component({
  selector: 'app-user-list-with-ngrx',
  templateUrl: './user-list-with-ngrx.component.html',
  styleUrls: ['./user-list-with-ngrx.component.scss'],
})
export class UserListWithNgrxComponent implements OnInit {
  state$: Observable<UserListState>;
  page = 1;
  limit = 10;
  total = 0;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.state$ = this.store.select((s) => s.userList);
    this.loadData();
  }

  loadData() {
    const param = new FindUsersParam();
    param.limit = this.limit;
    param.page = this.page;

    this.store.dispatch(LoadUserList({ param }));
  }

  onPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.limit = event.pageSize;

    this.loadData();
  }
}
