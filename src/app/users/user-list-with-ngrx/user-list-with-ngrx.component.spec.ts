import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListWithNgrxComponent } from './user-list-with-ngrx.component';

describe('UserListWithNgrxComponent', () => {
  let component: UserListWithNgrxComponent;
  let fixture: ComponentFixture<UserListWithNgrxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserListWithNgrxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListWithNgrxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
