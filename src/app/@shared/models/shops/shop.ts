import { Geometry } from 'geojson';

export interface Shop {
  name: string;
  location: Geometry;
}
