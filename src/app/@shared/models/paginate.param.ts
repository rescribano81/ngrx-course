export interface PaginateParam {
  page: number;
  limit: number;
}
