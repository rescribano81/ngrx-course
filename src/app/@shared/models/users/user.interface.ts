export interface User {
  username: string;
  email: string;
  phoneNumer: string;
  googleId: number;
  alias: string;
  photoUrl: string;
}
