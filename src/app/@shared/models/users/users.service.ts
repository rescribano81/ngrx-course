import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PaginateParam } from '@app/@shared/_dtos/paginate.param';
import { PaginateResult } from '@shared/_dtos/paginate-result';
import { Observable } from 'rxjs';
import { User } from './user.interface';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  findUsers(param: PaginateParam): Observable<PaginateResult<User>> {
    const url = '/users';
    const params = param.toHttpParams();

    return this.http.get<PaginateResult<User>>(url, { params });
  }
}
