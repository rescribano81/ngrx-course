import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { environment } from '@env/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  imports: [
    FlexLayoutModule,
    MaterialModule,
    CommonModule,

    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  declarations: [LoaderComponent],
  exports: [LoaderComponent],
})
export class SharedModule {}
