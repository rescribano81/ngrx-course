//import { PaginateOptions } from 'mongoose';
import { HttpParams } from '@angular/common/http';
import { PaginateParam } from '../paginate.param';

export class FindUsersParam extends PaginateParam {
  query: string;

  toHttpParams(): HttpParams {
    let params = super.toHttpParams();

    if (this.query) {
      params = params.set('query', this.query.toString());
    }

    return params;
  }
}
