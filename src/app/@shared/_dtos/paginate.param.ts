import { HttpParams } from '@angular/common/http';

export class PaginateParam {
  // select?: object | string;
  // sort?: object | string;
  // customLabels?: CustomLabels;
  // collation?: CollationOptions;
  // populate?: object[] | string[] | object | string | QueryPopulateOptions;
  // lean?: boolean;
  // leanWithId?: boolean;
  // offset?: number;
  page?: number;
  limit?: number;
  // read?: ReadOptions;
  // /* If pagination is set to `false`, it will return all docs without adding limit condition. (Default: `true`) */
  // pagination?: boolean;
  // projection?: any;
  // options?: QueryFindOptions;

  public constructor(init?: Partial<PaginateParam>) {
    Object.assign(this, init);
  }

  toHttpParams(): HttpParams {
    let params = new HttpParams();

    if (this.page) {
      params = params.set('page', this.page.toString());
    }

    if (this.limit) {
      params = params.set('limit', this.limit.toString());
    }

    return params;
  }
}
