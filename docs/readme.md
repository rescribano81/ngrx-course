---
**NOTA**
Para poder seguir este curso de forma cómoda puedes escribir `npm run docs` desde una terminal en la raiz del repositorio, y visitar http://localhost:4040
---

# Curso de ngrx

Este curso está orientado a ser una introducción a ngrx.

Ngrx es una implentación del patrón redux en angular, y puede ayudar a hacer nuestras aplicaciones web mas robustas y fácil de depurar

## Forma de uso de este curso

Antes de empezar el curso cada estudiante deberá hacer un fork del repositorio https://bitbucket.org/rescribano81/ngrx-course (Del cual solo tendrá acceso de lectura)
Luego deberá clonarlo en local con `git clone https://<USUARIO_ALUMNO>@bitbucket.org/<USUARIO_ALUMNO>/ngrx-course.git`

Ahora podrá abrir la documentación ejecutando `npm run docs` en la raiz de su repositorio local y visitando http://localhost:4040

En cada sesión se abordará un nuevo concepto de ngrx, y se propondrá un ejercicio práctico que el alumno deberá implementar en su propio repositorio.

El código base de cada ejercicio se encuentra ya en el repositorio.

## Índice

[[index]]
